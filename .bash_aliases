alias c='clear'

alias gfi='git flow init'
alias gsf='git flow feature start'
alias gff='git flow feature finish'
alias gfp='git flow feature publish'

